import bpy

class VANoteData:
    def __init__(self, video_file, audio_file,audio_file_offset,note_frame_start,volume=1.0,max_len_in_frames=500):
        self.video_file = video_file
        self.audio_file = audio_file
        self.audio_file_offset = audio_file_offset
        self.note_frame_start = note_frame_start
        self.volume = volume
        self.max_len_in_frames = max_len_in_frames

class VANotesData:
    def __init__(self):
        self.notes={}

class VAPart:
    '''WHAT THE HACKKS'''

    corresponding_notes = {
        'D-': 'C#',
        'E-': 'D#',
        'G-': 'F#',
        'A-': 'G#',
        'B-': 'A#',
    }

    def __init__(self, notesdata):
        self.notesdata = notesdata

    @staticmethod
    def make_pitched_note_name(initial_note_name, new_octave):
        return initial_note_name[:-1] + str(new_octave)

    def find_closest_available_note(self, initial_note_name, first_look_direction_up=False, max_offset=7):
        #that's really low and hight initial data, pitch will not help anyway
        # хотя название функции такого не предполагает
        # if initial_note_name[-1] == '0' or initial_note_name[-1] == '8':
        #     return None

        if initial_note_name in self.notesdata:
            return initial_note_name

        offset_module = 1
        while offset_module < max_offset:
            offset_val = offset_module * (1 if first_look_direction_up else -1)
            pitched_name1 = self.make_pitched_note_name(initial_note_name, int(initial_note_name[-1]) + offset_val)
            if pitched_name1 in self.notesdata:
                return pitched_name1

            pitched_name2 = self.make_pitched_note_name(initial_note_name, int(initial_note_name[-1]) - offset_val)
            if pitched_name2 in self.notesdata:
                return pitched_name2

            offset_module += 1

        return None


    def spawnnote(self, note, frametospawn, framesduration, channel_video, channel_audio, sequence_editor,
                  use_video=True, try_pitching_for_missing_notes=False, is_drum=False, use_volume_data=True):

        if framesduration < 4:# 4 was here
            framesduration = 3 #and 3 here
        #frametospawn +=50#################################ATTENTION
        note_pitch = 1


        if is_drum:
            note = int(note)

        if note not in self.notesdata:

            if is_drum:
                return

            #check if note id B-4 style to transform to A#4 style
            if '-' in note and len(note) == 3:
                unknown_note_part = note[:2]
                if unknown_note_part in self.corresponding_notes:
                    alternate_note_part = self.corresponding_notes[unknown_note_part]
                    note = alternate_note_part + note.split('-')[-1]

            #if still not helping
            if note not in self.notesdata:
                if try_pitching_for_missing_notes:
                    initial_note_octave = int(note[-1])
                    note = self.find_closest_available_note(note)

                    if note is None:
                        print('Serious fatal: {}'.format(str(initial_note_octave)))
                        return

                    new_note_octave = int(note[-1])
                    diff = new_note_octave - initial_note_octave
                    note_pitch = (diff + 1) if diff < 0 else 1/(1 + diff)
                else:
                    #self.report({'WARNING'}, note + ' not in note data')
                    return

        note_data = self.notesdata[note]
        frame_to_start_audio_strip = frametospawn - (note_data.note_frame_start - note_data.audio_file_offset)
        frame_final_start = frametospawn

        m = sequence_editor.sequences.new_sound(
            name=str(note),
            filepath=note_data.audio_file,
            channel=channel_audio, frame_start=frame_to_start_audio_strip)

        m.frame_offset_end = m.frame_final_end - frame_final_start - min(framesduration, note_data.max_len_in_frames)
        m.frame_offset_start = frame_final_start - frame_to_start_audio_strip
        m.pitch = note_pitch

        volume = 1.0

        if use_volume_data:
            volume = note_data.volume

        m.volume = volume

        if use_video:
            frame_to_start_video_strip = frametospawn - note_data.note_frame_start

            v = sequence_editor.sequences.new_movie(
                name=str(note),
                filepath=note_data.video_file,
                channel=channel_video, frame_start=frame_to_start_video_strip)

            v.frame_offset_end = v.frame_final_end - frametospawn - min(framesduration, note_data.max_len_in_frames)
            v.frame_offset_start = frametospawn - frame_to_start_video_strip
            return (m, v)

        return (m, None)


