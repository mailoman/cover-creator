import bpy
import operator
import os
from . import vapart


def collect_markers(video_seq, audio_seq):
    # strElem = sequence.strip_elem_from_frame(0)
    scene = bpy.context.scene  # .sequence_editor

    lastDotIndex = video_seq.filepath.rfind('.')
    fileName = bpy.path.abspath(video_seq.filepath[:lastDotIndex] + '.txt')

    with open(fileName, 'w') as f:
        video_path = bpy.path.abspath(video_seq.filepath)
        audio_path = bpy.path.abspath(audio_seq.sound.filepath)
        audio_offset = audio_seq.frame_start

        f.write(video_path + '\n')
        f.write(audio_path + '\n')
        f.write(str(audio_offset) + '\n')

        is_drum = scene.vapart_properties.is_drum_track

        if is_drum:
            for tm in scene.timeline_markers:
                note_name = tm.name
                note_length = ''
                if ' ' in tm.name:
                    split_data = tm.name.split(' ')
                    note_name = split_data[0]
                    note_length = split_data[1]
                    #RP_TODO потом добавить возможность добавлять громкость через блендер-маркеры
                f.write(note_name + ',' + str(tm.frame) + ',' + note_length + '\n')
        else:
            for tm in scene.timeline_markers:
                note_name, note_length = tm.name.split(' ')
                f.write(note_name + ',' + str(tm.frame) + ',' + note_length + '\n')


# collect_markers(bpy.context.scene.sequence_editor.active_strip)

#тут продолжить и сохранять ноты ударных в отдельный блок или завести поле is_drum в данные файла
def get_notes_from_files_in_dir(notes_dir, notes_audio_start_offset=0, is_drum=False):
    '''notes_audio_start_offset - small offset in frames of the original marker'''

    if notes_dir[-1] == '\\':
        notes_dir = notes_dir[:-1]

    abs_path = bpy.path.abspath(notes_dir)
    files = [f for f in os.listdir(abs_path) if os.path.isfile(os.path.join(abs_path, f)) and f[-4:] == '.txt']

    res = {}
    for f in files:
        with open(os.path.join(abs_path, f), 'r') as note_file:
            video_path = os.path.realpath(note_file.readline()).rstrip()
            audio_path = os.path.realpath(note_file.readline()).rstrip()
            audio_offset = note_file.readline().rstrip()

            for noteline in note_file:
                noteline = noteline.rstrip()

                #allow commenting with //
                if len(noteline) >= 2 and noteline[:2] == '//':
                    continue

                if is_drum:
                    notesplit_data = noteline.split(',')
                    finalnote = notesplit_data[0]
                    framestart = notesplit_data[1]
                    volume = 1.0
                    max_len_in_frames = 500

                    if len(notesplit_data) > 3:
                        volume = float(notesplit_data[3])

                    if len(notesplit_data) > 2:
                        max_len_in_frames = int(notesplit_data[2])

                    res[int(finalnote)] = vapart.VANoteData(video_path,
                                                            audio_path,
                                                            int(audio_offset),
                                                            int(framestart) + notes_audio_start_offset,
                                                            volume,
                                                            max_len_in_frames)
                else:
                    finalnote, framestart, framelen = noteline.split(',')
                    res[finalnote] = vapart.VANoteData (video_path, audio_path, int(audio_offset), int(framestart) + notes_audio_start_offset,max_len_in_frames= int(framelen))

    return res


class STRIP_OT_collectmarkernotes(bpy.types.Operator):
    bl_label = "Notes from markers"
    bl_idname = "strip.collectmarkernotes"
    bl_description = "Collect marker notes"

    # filepath = bpy.props.StringProperty(subtype="FILE_PATH")

    @staticmethod
    def _find_sound_and_movie_strips(context):
        movie_strip = None
        audio_strip = None
        for s in context.selected_sequences:
            if s.type == 'MOVIE':
                movie_strip = s
            elif s.type == 'SOUND':
                audio_strip = s

        return (movie_strip, audio_strip)

    @classmethod
    def poll(cls, context):
        return len(context.selected_sequences) == 2

    def execute(self, context):
        movie_strip, audio_strip = self._find_sound_and_movie_strips(context)

        if movie_strip and audio_strip:
            collect_markers(movie_strip, audio_strip)
            return {'FINISHED'}
        else:
            self.report({'WARNING'}, 'You must have selected both movie and sound strips only')
            return {'CANCELLED'}


class STRIP_OT_spawnmarkernotes_and_strips(bpy.types.Operator):
    bl_label = "Markers and strips from notes"
    bl_idname = "strip.spawnmarkernotes_and_strips"
    bl_description = "Spawn marker notes and strips"

    # filepath = bpy.props.StringProperty(subtype="FILE_PATH")
    @classmethod
    def poll(cls, context):
        return len(context.scene.vapart_notedatafile) > 4 and context.scene.vapart_notedatafile[-3:] == 'txt'

    def execute(self, context):
        file_name = context.scene.vapart_notedatafile

        with open(bpy.path.abspath(file_name), 'r') as f:
            video_path = os.path.realpath(f.readline().rstrip())
            audio_path = os.path.realpath(f.readline().rstrip())
            audio_offset = int(f.readline().rstrip())

            m = bpy.context.scene.sequence_editor.sequences.new_sound(
                name=file_name,
                filepath=audio_path,
                channel=2, frame_start=audio_offset)

            v = bpy.context.scene.sequence_editor.sequences.new_movie(
                name=file_name,
                filepath=video_path,
                channel=1, frame_start=0)

            is_drum = context.scene.vapart_properties.is_drum_track
            for noteline in f:
                noteline = noteline.rstrip()

                # allow commenting with //
                if len(noteline) >= 2 and noteline[:2] == '//':
                    continue

                if is_drum:
                    split_data = noteline.split(',')
                    if noteline.count(',') > 3:
                        self.report({'WARNING'}, "Unknown note formatting")
                        continue

                    marker_name = split_data[0]
                    framestart = split_data[1]

                    if len(split_data) > 2:
                        marker_name += ' ' + split_data[2]

                    marker = context.scene.timeline_markers.new(marker_name, int(framestart))
                else:
                    if noteline.count(',') != 2:
                        self.report({'WARNING'}, "Unknown note formatting")
                        continue
                    finalnote, framestart, framelen = noteline.split(',')
                    marker = context.scene.timeline_markers.new(finalnote + ' ' + framelen, int(framestart))

        return {'FINISHED'}
        # res, movie_strip, audio_strip = self.find_valid_strips(context)
        #
        # if res:
        #     collect_markers(movie_strip, audio_strip)
        #     return {'FINISHED'}
        # else:
        #     self.report({'WARNING'}, 'You must have selected both movie and sound strips only')
        #     return {'CANCELLED'}


class STRIP_OT_setnotemarkerlength(bpy.types.Operator):
    bl_label = "Apply prev marker length"
    bl_idname = "strip.setnotemarkerlength"
    bl_description = "Set last market note length to math current timemarker"

    # filepath = bpy.props.StringProperty(subtype="FILE_PATH")

    @classmethod
    def poll(cls, context):
        is_drum = context.scene.vapart_properties.is_drum_track
        return (not is_drum) and len(context.scene.timeline_markers) > 0

    def execute(self, context):
        # strip = context.scene.sequence_editor.active_strip
        curFrame = context.scene.frame_current

        markers = sorted(context.scene.timeline_markers.values(), key=operator.attrgetter('frame'))

        found_marker = markers[0]
        for m in markers:
            if m.frame > curFrame:
                break
            found_marker = m

        foundLen = curFrame - found_marker.frame

        if ' ' in found_marker.name:
            found_marker.name = found_marker.name.split(' ')[0] + ' ' + str(foundLen)
        else:
            found_marker.name = found_marker.name + ' ' + str(foundLen)

        return {'FINISHED'}

# bpy.utils.register_class(CollectNotesDataFromStripMarkers)
