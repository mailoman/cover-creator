import bpy
from operator import attrgetter

class VAHELPER_OT_meta_from_selected_audios(bpy.types.Operator):
    bl_label = "Collapse audios"
    bl_idname = "vahelper.meta_from_selected_audios"
    bl_description = "Collapse selected audios"

    # filepath = bpy.props.StringProperty(subtype="FILE_PATH")

    @staticmethod
    def find_valid_strips(context):
        res = []

        for s in context.selected_sequences:
            # if s.type == 'MOVIE':
            #     movie_strip = s
            if s.type == 'SOUND':
                res.append(s)

        return res

    @classmethod
    def poll(cls, context):
        return len(context.selected_sequences) > 0

    def execute(self, context):
        audio_strips = self.find_valid_strips(context)
        bpy.ops.sequencer.select_all(action='DESELECT')
        for s in audio_strips:
            s.select = True

        bpy.ops.sequencer.meta_make()

        return {'FINISHED'}


class VAHELPER_OT_set_proxies_to_all_selected_submovies(bpy.types.Operator):
    bl_label = "Set proxies"
    bl_idname = "vahelper.set_proxies_to_all_selected_submovies"
    bl_description = "Set proxies to all selected submodules"

    @classmethod
    def poll(cls, context):
        return len(context.selected_sequences) > 0

    def inner_execute(self, strips):
        for s in strips:
            if s.type == 'META':
                self.inner_execute(s.sequences)
            elif s.type == 'MOVIE':
                s.use_proxy = True
                s.proxy.quality = 20


    def execute(self, context):
        self.inner_execute(context.selected_sequences)
        return {'FINISHED'}


class VAHELPER_OT_select_sound_chord_strips(bpy.types.Operator):
    bl_label = "Select same starting strips"
    bl_idname = "vahelper.select_only_sound_chord_strips"
    bl_description = "Select same starting strips"

    @classmethod
    def poll(cls, context):
        return len(context.selected_sequences) > 1

    def inner_execute(self, strips):
        bpy.ops.sequencer.select_all(action='DESELECT')

        strips_of_type = [vs for vs in strips if vs.type == 'SOUND']
        strips_of_type = sorted(strips_of_type, key=attrgetter('frame_final_start', 'channel'))

        i = 0
        cur_chord = []
        while i < len(strips_of_type):
            if len(cur_chord) == 0 or cur_chord[-1].frame_final_start == strips_of_type[i].frame_final_start:
                cur_chord.append(strips_of_type[i])
                i += 1

                if i == len(strips_of_type) and len(cur_chord) > 1:
                    for s in cur_chord:
                        s.select = True

                continue

            if len(cur_chord) > 1 and cur_chord[-1].frame_final_start != strips_of_type[i].frame_final_start:
                for s in cur_chord:
                    s.select = True
                cur_chord.clear()
                continue

            cur_chord.clear()
            cur_chord.append(strips_of_type[i])
            i += 1

    def execute(self, context):
        self.inner_execute(context.selected_sequences)
        return {'FINISHED'}