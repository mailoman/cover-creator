import bpy
import operator


class STRIP_OT_change_volume_add(bpy.types.Operator):
    bl_label = "Volume modify add"
    bl_idname = "strip.change_volume_add"
    bl_description = "Adds to all sound strips volume"

    @classmethod
    def poll(cls, context):
        return len(context.selected_sequences) > 0

    @staticmethod
    def do_work_recursively(sequences, change_value):
        for sequence in sequences:
            if sequence.type == 'SOUND':
                sequence.volume += change_value
            elif sequence.type == 'META':
                STRIP_OT_change_volume_add.do_work_recursively(sequence.sequences, change_value)

    def execute(self, context):
        self.do_work_recursively(context.selected_sequences, context.scene.vapart_volume_modify_add)
        return {'FINISHED'}


class STRIP_OT_change_volume_multiply(bpy.types.Operator):
    bl_label = "Volume modify multiply"
    bl_idname = "strip.change_volume_multiply"
    bl_description = "Multiply to all sound strips volume"

    @classmethod
    def poll(cls, context):
        return len(context.selected_sequences) > 0

    @classmethod
    def do_work_recursively(cls,sequences, change_value):
        for sequence in sequences:
            if sequence.type == 'SOUND':
                sequence.volume *= change_value
            elif sequence.type == 'META':
                cls.do_work_recursively(sequence.sequences, change_value)

    def execute(self, context):
        STRIP_OT_change_volume_multiply.do_work_recursively(context.selected_sequences, context.scene.vapart_volume_modify_multiply)
        return {'FINISHED'}


class STRIP_OT_change_volume_indent(bpy.types.Operator):
    bl_label = "Volume modify indent"
    bl_idname = "strip.change_volume_indent"
    bl_description = "Indent to all sound strips volume"

    @classmethod
    def poll(cls, context):
        return len(context.selected_sequences) > 0

    @staticmethod
    def do_work_recursively(sequences, change_value):
        for sequence in sequences:
            if sequence.type == 'SOUND':
                sequence.volume = change_value
            elif sequence.type == 'META':
                STRIP_OT_change_volume_indent.do_work_recursively(sequence.sequences, change_value)

    def execute(self, context):
        STRIP_OT_change_volume_indent.do_work_recursively(context.selected_sequences, context.scene.vapart_volume_modify_indent)
        return {'FINISHED'}